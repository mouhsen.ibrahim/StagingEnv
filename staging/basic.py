import sys, os
from signal import SIGTERM
import logging
import json

logging.basicConfig(format="%(asctime)s - %(levelname)s - %(name)s - %(message)s", filename="deploy.log")

class Log:
    def __init__(self,name=__name__, level=logging.DEBUG):
        self.logger = logging.getLogger(name)
        self.logger.setLevel(level)

    def logWarning(self, message):
        self.logger.warning(message)

    def logDebug(self, message):
        self.logger.debug(message)

    def logInfo(self, message):
        self.logger.info(message)

    def logError(self, message):
        self.logger.error(message)

    def logCritical(self, message):
        self.logger.critical(message)

logger = Log(__name__)

class Stage:
    def __init__(self, name, port, workTree, branch, owner):
        self.name = name
        self.port = port
        self.workTree = workTree
        self.branch = branch
        self.owner = owner

    def __eq__(self, stage):
        return ((self.name == stage.name ) and (int(self.port) == int(stage.port)))

    def __str__(self):
        return "Stage: <name: %s port %d workTree %s branch %s owner %s>" % (self.name, int(self.port), self.workTree, self.branch, self.owner)

stages = []

def setUpStage(name, port, workTree, branch, owner):
    stage = Stage(name, port, workTree, branch, owner)
    for st in stages:
        if st == stage:
            from staging.dockerBackend import Docker
            docker = Docker()
            if docker.hasContainerWithName(st.name):
                return st
            else:
                deleteStage(st)
                break
    from staging.dockerBackend import Docker
    from staging.configFile import config
    docker = Docker()
    stage = docker.run(config.get("docker", "image"), name, port,workTree, branch, owner)
    logger.logInfo("Adding this new stage %s" % stage)
    stages.append(stage)
    return stage

def deleteStage(stage):
    for st in stages:
        if st.name == stage.name:
            stages.remove(st)

def fillStages():
    from staging.configFile import config
    stagesFile = config.get("stage", "file")
    try:
        data = open(stagesFile).read()
    except IOError as e:
        return
    try:
        jsonData = json.loads(data)
    except ValueError as e:
        return
    from staging.dockerBackend import Docker
    docker = Docker()
    for stage in jsonData:
        if docker.hasContainerWithName(stage["name"]):
            stages.append(Stage(stage["name"], stage["port"], stage["workTree"], stage["branch"], stage["owner"]))

def writeStages():
    from staging.configFile import config
    stagesFile = config.get("stage", "file")
    f = open(stagesFile, "w")
    data = []

    for stage in stages:
        data.append({"name": stage.name, "port": stage.port, "workTree": stage.workTree, "branch": stage.branch, "owner": stage.owner})
    f.write(json.dumps(data))
    f.close()
