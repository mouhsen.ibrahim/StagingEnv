from staging.basic import Log

import ConfigParser
import os
import sys

logger = Log(__name__)

class Config:
    sections = ["docker", "gitlab", "git", "server", "stage", "env", "API"]
    options = {"docker" : ["socket", "image"],
        "gitlab" : ["server", "access_token", "project_name"],
        "git" : ["repos", "trees"],
        "server" : ["host", "port"],
        "stage" : ["host", "file"],
        "env" : ["path"],
        "API" : ["port"]
    }
    def __init__(self, filename):
        self.config = ConfigParser.ConfigParser()
        try:
            self.config.readfp(open(filename))
            for section in self.sections:
                if not self.config.has_section(section):
                    print("Section %s is missing" % section)
                    raise Exception
                else:
                    for option in self.options[section]:
                        if not self.config.has_option(section, option):
                            print ("Option %s is missing from section %s" % (option, section))
                            raise Exception
        except Exception as e:
            print(e)
            print("configuration file %s not found" % filename)
            raise Exception

    def get(self, section, name):
        try:
            return self.config.get(section, name)
        except Exception as e:
            return None

HOME_DIR = os.environ["HOME"]
try:
    config = Config(HOME_DIR + "/.staging/staging.ini")
except Exception as e:
    print("Error")
    sys.exit(1)
