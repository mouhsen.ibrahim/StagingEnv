import unittest

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from staging.dockerbackend import Docker, NoImageError, NameConflictError, NoContainerError, PortConflictError

class TestDockerBackend(unittest.TestCase):
    def test_createContainer(self):
        client = Docker()
        with self.assertRaises(NoImageError):
            client.createContainer("test_con", 1234, '/test', 'test_img')
        client.createContainer("test_con", 1234, '/test', 'ubuntu')
        with self.assertRaises(NameConflictError):
            client.createContainer("test_con", 1234, '/test', 'ubuntu')
        client.removeContainer("test_con")

    def test_startContainer(self):
        client = Docker()

        client.createContainer("test_con", 12345, '/test', 'ubuntu')
        client.startContainer("test_con")

        # test PortConflictError port 1234 is already in use by nginx
        client.createContainer("test_con1", 1234, '/test', 'ubuntu')
        with self.assertRaises(PortConflictError):
            client.startContainer("test_con1")

        client.removeContainer("test_con")
        client.removeContainer("test_con1")

    def test_removeContainer(self):
        client = Docker()

        client.createContainer("test_con", 12345, '/test', 'ubuntu')
        # remove a not running container
        client.removeContainer("test_con")

        client.createContainer("test_con", 12345, '/test', 'ubuntu')
        client.startContainer("test_con")
        # remove a running container
        client.removeContainer("test_con")

        # test creating a new container with a random port chosen by docker
        client.createContainer("test_con", None, "/test", "ubuntu")
        client.startContainer("test_con")

        client.removeContainer("test_con")

    def test_stopContainer(self):
        client = Docker()

        client.stopContainer("test_con")

    def test_hasContainer(self):
        client = Docker()

        # make sure the container is removed
        try:
            client.removeContainer("test_con")
        except Exception as e:
            pass

        exists = client.hasContainer("test_con")

        self.assertFalse(exists)

        # now create the container
        client.createContainer("test_con", 12345, '/test', 'ubuntu')

        exists = client.hasContainer("test_con")

        self.assertTrue(exists)

        # start the container
        client.startContainer("test_con")


        exists = client.hasContainer("test_con")

        self.assertTrue(exists)

        client.removeContainer("test_con")

    def test_listContainers(self):
        # test the listContainers method, this test cannot be created perfectly
        # because the development environment I am setting up here might include
        # some containers created by others so I just try to count containers number
        # then create one and check if the number has increased by one and that
        # the newly crated container is in the results.
        # NOTE: this test is not perfect right now and cannot be improved unless
        # a clean dev env is used.

        client = Docker()

        try:
            client.removeContainer("test_con")
        except Exception as e:
            pass

        containers = client.listContainers(True)
        size = len(containers)

        client.createContainer("test_con", 12345, '/test', 'ubuntu')

        containers = client.listContainers(True)
        newSize = len(containers)

        # I hope no other user created a container before this is ran....
        self.assertTrue((newSize - size) == 1)

        exists = False
        for container in containers:
            if container["Names"][0] == "/test_con":
                exists = True
                break

        self.assertTrue(exists)

        client.removeContainer("test_con")

        containers = client.listContainers(False)
        size = len(containers)

        client.createContainer("test_con", 12345, '/test', 'ubuntu')
        client.startContainer("test_con")

        containers = client.listContainers(False)
        newSize = len(containers)

        # I hope no other user created a container before this is ran....
        self.assertTrue((newSize - size) == 1)

        exists = False
        for container in containers:
            if container["Names"][0] == "/test_con":
                exists = True
                break

        self.assertTrue(exists)

        client.removeContainer("test_con")

    def test_getPort(self):
        client = Docker()

        client.createContainer("test_con", 12345, '/test', 'ubuntu')
        client.startContainer("test_con")

        self.assertTrue(client.hasContainer("test_con"))
        self.assertEqual(client.getPort("test_con"), 12345)
        with self.assertRaises(NoContainerError):
            client.getPort("test_con2")

        client.createContainer("test_con2", 12345, '/test', 'ubuntu')
        with self.assertRaises(NoContainerError):
            client.getPort("test_con2")
        client.removeContainer("test_con")
        client.removeContainer("test_con2")
