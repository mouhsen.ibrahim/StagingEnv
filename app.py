#!/usr/bin/env python
from staging.basic import Log
from staging.Server import HTTPServer
from staging.api import API
from staging.configFile import config

import argparse
import os

from staging.daemon import Daemon

logger = Log(__name__)

class MyDaemon(Daemon):
    def run(self):
        port = config.get("API", "port")
        api = API(int(port))
        api.start()
        httpd = HTTPServer()

    def status(self):
        try:
            pf = file(self.pidfile,'r')
        except IOError as e:
            return False
        pid = int(pf.read().strip())
        pf.close()
        try:
            os.kill(pid, 0)
        except OSError as e:
            return False
        else:
            return True

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", choices=["start","stop","restart", "status"])
    args = parser.parse_args()

    if args.d:
        HOME_DIR = os.environ["HOME"]
        STDOUT = "%s/.staging/staging.out" % HOME_DIR
        STDERR = "%s/.staging/staging.err" % HOME_DIR
        PID = "%s/.staging/staging.pid" % HOME_DIR
        daemon = MyDaemon(PID, '/dev/null', STDOUT, STDERR)
        if args.d == "start":
            daemon.start()
        elif args.d == "stop":
            daemon.stop()
        elif args.d == "restart":
            daemon.restart()
        else:
            if daemon.status():
                print("daemon is running")
            else:
                print("daemon is NOT running")
    else:
        port = config.get("API", "port")
        api = API(int(port))
        api.start()
        httpd = HTTPServer()


if __name__ == "__main__":
    main()
